package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DishFlavorMapper {
    /**
     * 保存菜品对应的口味信息
     * @param flavorList
     */
    void saveDishFlavor(List<DishFlavor> flavorList);

    /**
     * 根据菜品id的集合删除对应的口味
     * @param ids
     */
    void delByDishIds(List<Long> ids);

    /**
     * 删除菜品原有的口味
     * @param dishId
     */
    void deleteByDishId(Long dishId);


    List<DishFlavor> getByDishId(Long dishId);

    void deleteByDishIds(List<Long> ids);

    /**
     * 根据菜品id查询口味信息
     * @param dishId
     * @return
     */
    List<DishFlavor> findByDishId(Long id);
}

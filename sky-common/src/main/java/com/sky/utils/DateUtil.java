package com.sky.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DateUtil {
    public static List<LocalDate> getRangeDate(LocalDate start,LocalDate end){
        //获取到了日期的区间 => 获取这个期间每一天
        ArrayList<LocalDate> dateList = new ArrayList<>();
        dateList.add(start);
        while (!start.equals(end)){
            //获取下一天
            start = start.plusDays(1l);
            dateList.add(start);
        }
        return dateList;
    }


}

package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 演示：
 */
@Api(tags = "菜品相关接口")
@RestController
@RequestMapping("/admin/dish")
public class DishController {

    @Autowired
    DishService dishService;

    /**
     * 上传菜品信息
     * @param dishDTO
     * @return
     */
    @ApiOperation("菜品上传")
    @PostMapping
    public Result save(@RequestBody DishDTO dishDTO){
        dishService.save(dishDTO);
        return Result.success();
    }

    /**
     * 菜品分页查询
     * @param PageQueryDTO
     * @return
     */
    @ApiOperation("菜品分页查询")
    @GetMapping("/page")
    public Result<PageResult> page(DishPageQueryDTO PageQueryDTO){
        PageResult pageResult = dishService.page(PageQueryDTO);
        return Result.success(pageResult);
    }

    /**
     * 批量删除菜品
     * @param
     * @return
     */
    @ApiOperation("批量删除菜品")
    @DeleteMapping
    public Result delByIds(@RequestParam List<Long> ids){
        dishService.delByIds(ids);
        return Result.success();
    }

    /**
     * 根据id查询菜品以及口味信息
     * @param id
     * @return
     */
    @ApiOperation("根据id查询菜品以及口味信息")
    @GetMapping("/{id}")
    public Result<DishVO> getById(@PathVariable Long id){
        DishVO dishVO = dishService.getByIdWithFlavor(id);
        return Result.success(dishVO);
    }

    /**
     * 修改菜品
     * @param dishDTO
     * @return
     */
    @ApiOperation("修改菜品")
    @PutMapping
    public Result update(@RequestBody DishDTO dishDTO){
        dishService.updateWithFlavor(dishDTO);
        return Result.success();
    }

    /**
     * 菜品启售停售
     * @param status
     * @param id
     * @return
     */
    @ApiOperation("菜品启售停售")
    @PostMapping("/status/{status}")
    public Result<String> startOrStop(@PathVariable Integer status,Long id){
        dishService.startOrStop(status,id);
        return Result.success();
    }

    /**
     * 套餐管理-新增套餐-添加菜品-菜品分类(补充)
     * 通过指定的菜品分类ID查询相应的菜品列表
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    public Result<List<Dish>> findListById(Long categoryId){
        List<Dish> dishList = dishService.findListById(categoryId);
        return Result.success(dishList);
    }

}

package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.ReactiveSubscription;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 演示：
 */
@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    SetmealMapper setmealMapper;

    @Autowired
    DishMapper dishMapper;

    @Autowired
    SetmealDishMapper setmealDishMapper;
    /**
     * 添加套餐
     * @param setmealDTO
     */
    @Transactional  //用到多表时,开启声明式事物
    @Override
    public void add(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.addSetmeal(setmeal);

        //获取生成的套餐id
        Long setmealId = setmeal.getId();

        //套餐菜品
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        setmealDishes.forEach(setmealDish -> {
            //setmealDish.setSetmealId(setmealId);
            setmealDish.setSetmealId(setmeal.getId());
        });

        //保存套餐和菜品的关联关系
        setmealMapper.addSetmealDish(setmealDishes);
    }

    /**
     * 分页查询
     * @param pageQueryDTO
     * @return
     */
    @Override
    public PageResult page(SetmealPageQueryDTO pageQueryDTO) {
        PageHelper.startPage(pageQueryDTO.getPage(),pageQueryDTO.getPageSize());
        Page<SetmealVO> page = setmealMapper.page(pageQueryDTO);
        return new PageResult(page.getTotal(),page.getResult());
    }

    /**
     * 删除套餐
     * @param ids
     */
    @Transactional
    @Override
    public void deleteById(List<Long> ids) {
        //1.套餐起售不能删除
        ids.forEach(id -> {
            //查看当前套餐状态
            Setmeal setmeal = setmealMapper.getById(id);
            if (StatusConstant.ENABLE == setmeal.getStatus()){
                //启售,不能删,抛异常
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }
        });
        //删除套餐基本数据
        setmealMapper.deleteByIds(ids);
        //删除套餐关联的菜品数据
        setmealMapper.deleteBySetmealIds(ids);
    }

    /**
     * 套餐起售停售
     * @param status
     * @param id
     */
    @Transactional
    @Override
    public void changeStatus(Integer status, Long id) {
        Setmeal setmeal = new Setmeal();
        setmeal.setStatus(status);
        setmeal.setId(id);
        //停售
        if (status == StatusConstant.DISABLE){  //套餐状态 = 0
            setmealMapper.updateSetmeal(setmeal);
        }

        //起售
        List<SetmealDish>  setmealDishes = setmealDishMapper.findBySetmealId(id);  //根据id查询套餐菜品表中的套餐对象
        for (SetmealDish setmealDish : setmealDishes) {  //遍历套餐对象
            Dish dish = dishMapper.findById(setmealDish.getDishId()); //获取套餐中的菜品id
                if (dish.getStatus() == 0){  //判断菜品id的状态
                    throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
                }
            }
            setmealMapper.update(setmeal);
        }

    /**
     * 修改套餐
     * @param setmealDTO
     */
    @Transactional
    @Override
    public void updateSetmeal(SetmealDTO setmealDTO) {
        //1.修改套餐的基本信息
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.updateSetmeal(setmeal);  //更新套餐基本信息
        //2.删除套餐原有的菜品

        //删除套餐和菜品的关联
        Long setmealId = setmealDTO.getId();
        setmealMapper.deleteBySetmealId(setmealId);
        //setmealDishMapper.deleteBySetmealId(setmealDTO.getId());
        //3.再添加新的菜品
        List<SetmealDish> setmealDishList = setmealDTO.getSetmealDishes();
        if (setmealDishList != null && setmealDishList.size() >0){
            setmealDishList.forEach(setmealDish -> {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                setmealDish.setSetmealId(setmealId);
            });
            //4.重新插入套餐和菜品的关系
            setmealMapper.saveSetmealDish(setmealDishList);
        }

    }

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @Override
    public SetmealVO findById(Long id) {
        return setmealMapper.findById(id);
    }

    /**
     * 条件查询
     * @param setmealDTO
     * @return
     */
    public List<Setmeal> list(SetmealDTO setmealDTO) {
        List<Setmeal> list = setmealMapper.list(setmealDTO);
        return list;
    }

    @Override
    public List<DishItemVO> getDishItemById(Long id) {
        return null;
    }
}

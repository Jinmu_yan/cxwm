package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    /**
     *购物车查询
     * @param shoppingCart
     * @return
     */
    ShoppingCart findShoppingCartOptions(ShoppingCart shoppingCart);

    /**
     * 更新商品数量
     * @param shoppingCartDB
     */
    void updateNumberById(ShoppingCart shoppingCartDB);

    /**
     * 将购物车数据存入数据库
     * @param shoppingCart
     */
    void save(ShoppingCart shoppingCart);

    /**
     * 根据用户id查询购物车表
     * @param userId
     * @return
     */
    List<ShoppingCart> findByUserId(Long userId);

    /**
     * 根据用户id清空购物车表
     * @param userId
     */
    void deleteById(Long userId);

    /**
     * 购物车查询数据 删除
     * @param condition
     * @return
     */
    List<ShoppingCart> findShoppingCartByCondiotion(ShoppingCart condition);

    /**
     * 更新商品数量
     * @param id
     * @param i
     */
    void updateNumber(Long id, int i);

    /**
     * 根据id删除商品
     * @param id
     */
    void deleteShoppingCart(Long id);


}

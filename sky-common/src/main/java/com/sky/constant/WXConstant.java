package com.sky.constant;

/**
 * 演示：
 */
public class WXConstant {
    public static final String APP_ID = "appid";
    public static final String APP_SECRET = "secret";
    public static final String JS_CODE = "js_code";
    public static final String GRANT_TYPE = "grant_type";
    public static final String OPEN_ID = "openid";

}

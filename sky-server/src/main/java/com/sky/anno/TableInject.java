package com.sky.anno;

import com.sky.enumeration.OperationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 演示：自定义注解
 *      配合元注解
 */
@Target(ElementType.METHOD)  //注解添加位置 => 加在方法上面
@Retention(RetentionPolicy.RUNTIME)  //声明注解的作用时期  => 运行时
public @interface TableInject {

    /**
     * 操作类型
     *      OperationType.UPDATE    =>    更新人/更新时间
     *      OperationType.INSERT    =>    创建人/创建时间/更新人/更新时间
     */
    OperationType value();
}

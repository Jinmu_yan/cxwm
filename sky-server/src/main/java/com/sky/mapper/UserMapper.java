package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

@Mapper
public interface UserMapper {

    /**
     * 根据openid去数据库中查找user用户,看是否已经登陆过
     * @param openId
     * @return
     */
    User findUserByOpenid(String openId);

    /**
     * 注册user,并保存在数据库中
     * @param user
     */
    void save(User user);

    /**
     * 根据用户id查询user表数据
     * @param userId
     * @return
     */
    User findById(Long userId);

    /**
     * 统计指定时间区间内的用户数据
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}

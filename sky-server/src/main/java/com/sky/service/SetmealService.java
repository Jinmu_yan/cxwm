package com.sky.service;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface SetmealService {

    /**
     * 添加套餐
     * @param setmealDTO
     */
    void add(SetmealDTO setmealDTO);

    /**
     * 分页查询
     * @param pageQueryDTO
     * @return
     */
    PageResult page(SetmealPageQueryDTO pageQueryDTO);

    /**
     * 删除套餐
     * @param ids
     */
    void deleteById(List<Long> ids);

    /**
     * 套餐起售停售
     * @param status
     * @param id
     */
    void changeStatus(Integer status, Long id);

    /**
     * 修改套餐
     * @param setmealDTO
     */
    void updateSetmeal(SetmealDTO setmealDTO);

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    SetmealVO findById(Long id);

    List<Setmeal> list(SetmealDTO setmealDTO);
    //TODO 请求??
    List<DishItemVO> getDishItemById(Long id);
}

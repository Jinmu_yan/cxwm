package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.xiaoymin.knife4j.core.util.CollectionUtils;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.*;
import com.sky.entity.*;
import com.sky.exception.AddressBookBusinessException;
import com.sky.exception.OrderBusinessException;
import com.sky.exception.ShoppingCartBusinessException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.service.OrderService;
import com.sky.utils.HttpClientUtil;
import com.sky.utils.WeChatPayUtil;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import com.sky.webSocket.WebSocketServer;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.sky.entity.Orders.CANCELLED;
import static com.sky.entity.Orders.TO_BE_CONFIRMED;

/**
 * 演示：
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    AddressBookMapper addressBookMapper;
    @Autowired
    ShoppingCartMapper shoppingCartMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderDetailMapper orderDetailMapper;
    @Autowired
    WeChatPayUtil weChatPayUtil;
    @Autowired
    private WebSocketServer webSocketServer;

    @Value("${sky.shop.address}")
    private String shopAddress;

    @Value("${sky.baidu.ak}")
    private String ak;

    /**
     * 用户下单
     *
     * @param ordersSubmitDTO
     * @return
     */
    @Transactional  //多表
    @Override
    public OrderSubmitVO submitOrder(OrdersSubmitDTO ordersSubmitDTO) {
        Long userId = BaseContext.getCurrentId();  //拿到用户id
        //1.查询地址信息(地址簿为空、购物车数据为空)
        AddressBook addressBook = addressBookMapper.findByIdAndUserId(ordersSubmitDTO.getAddressBookId(), userId); //根据地址表id和用户id查询地址信息
        if (addressBook == null) {
            //抛出业务异常--用户地址为空
            throw new AddressBookBusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }

        //检查用户的收货地址是否超出配送范围
        checkOutOfRange(addressBook.getCityName() + addressBook.getDistrictName() + addressBook.getDetail());


        //2.查询当前用户的购物车有数据
        //ShoppingCart shoppingCart = new ShoppingCart();
        //shoppingCart.setUserId(userId);  //关联
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.findByUserId(userId);//根据用户id查询购物车数据

        //判断购物车里有没有数据
        if (shoppingCartList == null || shoppingCartList.size() == 0) {
            //抛异常--购物车为空
            throw new ShoppingCartBusinessException(MessageConstant.SHOPPING_CART_IS_NULL);
        }

        //3.向订单表存入数据
        Orders orders = new Orders();
        BeanUtils.copyProperties(ordersSubmitDTO, orders);
        //绑定数据(绿线 需要手动添加的那部分)
        orders.setNumber(String.valueOf(System.currentTimeMillis()));  //使用时间戳作为订单号
        orders.setStatus(Orders.PENDING_PAYMENT);  //订单状态:1 待付款
        orders.setUserId(userId);
        orders.setOrderTime(LocalDateTime.now());
        orders.setPayStatus(Orders.UN_PAID);   //支付状态:0未支付

        User user = userMapper.findById(userId);  //根据用户id查询user表数据
        orders.setUserName(user.getName());

        orders.setPhone(addressBook.getPhone());
        orders.setConsignee(addressBook.getConsignee());
        orders.setAddress((addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));
        //存储订单表数据
        orderMapper.save(orders);

        //4.存订单详情
        //注意:订单详情数据来自于 购物车数据
        List<OrderDetail> orderDetailList = shoppingCartList.stream()
                .map(shoppingCart -> {
                    OrderDetail orderDetail = new OrderDetail();
                    BeanUtils.copyProperties(shoppingCart, orderDetail);
                    //购物车表中的id和订单详情表的id不能对应,需要设置为null
                    orderDetail.setId(null);
                    //设置订单id
                    orderDetail.setOrderId(orders.getId());
                    return orderDetail;
                }).collect(Collectors.toList());
        //存储订单详情表数据
        orderDetailMapper.saveDetailList(orderDetailList);
        //5.清空当前用户的购物车数据
        shoppingCartMapper.deleteById(userId);
        //6.封装VO数据返回结果
        return OrderSubmitVO.builder()
                .id(orders.getId())
                .orderAmount(orders.getAmount())
                .orderNumber(orders.getNumber())
                .orderTime(orders.getOrderTime())
                .build();
    }

    /**
     * 根据订单号修改订单
     *
     * @param orderNum
     */
    @Override
    public void update(String orderNum) {
        //1.根据订单号查询订单,如果查不到,则抛出异常
        Orders orders = orderMapper.findByOrderNum(orderNum);
        if (orders == null) {
            throw new OrderBusinessException(MessageConstant.ORDER_NOT_FOUND); //订单不存在异常
        }
        //2.修改订单状态
        orders.setStatus(TO_BE_CONFIRMED);  //待接单
        orders.setCheckoutTime(LocalDateTime.now());  //结账时间
        orders.setPayStatus(Orders.PAID); //已支付
        orderMapper.update(orders);
    }

    /**
     * 历史订单查询
     *
     * @param pageNum
     * @param pageSize
     * @param status
     * @return
     */
    @Override
    public PageResult page(int pageNum, int pageSize, Integer status) {
        //使用PageHelper进行分页
        PageHelper.startPage(pageNum, pageSize);
        //创建对象 获取用户id 和 订单状态
        OrdersPageQueryDTO ordersPageQueryDTO = new OrdersPageQueryDTO();
        ordersPageQueryDTO.setUserId(BaseContext.getCurrentId());  //获取用户id
        ordersPageQueryDTO.setStatus(status);  //订单状态
        //进行分页条件查询
        Page<Orders> page = orderMapper.page(ordersPageQueryDTO);
        //创建集合存储结果
        List<OrderVO> list = new ArrayList();
        //遍历结果
        if (page != null && page.getTotal() > 0) {
            for (Orders orders : page) {
                Long orderId = orders.getId();
                //根据订单id查询订单明细
                List<OrderDetail> orderDetails = orderDetailMapper.getByOrderId(orderId);
                //创建VO对象,将订单和明细 填充进去
                OrderVO orderVO = new OrderVO();
                BeanUtils.copyProperties(orders, orderVO);
                orderVO.setOrderDetailList(orderDetails);
                list.add(orderVO);
            }
        }
        //最终结果以PageResult对象返回,里面包含订单总数 和 订单列表
        return new PageResult(page.getTotal(), list);
    }

    /**
     * 根据id查询订单详情
     *
     * @param id
     * @return
     */
    @Override
    public OrderVO findOrderDetail(Long id) {
        //1.根据id查询订单详情
        Orders orders = orderMapper.findById(id);
        //2.查询订单中包含那些商品
        List<OrderDetail> orderDetailList = orderDetailMapper.getByOrderId(orders.getId());
        //3.创建VO对象,将订单和明细 填充进去
        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(orders, orderVO);
        orderVO.setOrderDetailList(orderDetailList);
        return orderVO;
    }

    /**
     * 取消订单
     *
     * @param id
     */
    @Override
    public void cancelOrder(Long id) throws Exception {
        //根据订单id查询订单详情
        Orders ordersDB = orderMapper.findById(id);

        //1.判断订单号是否存在
        if (ordersDB == null) {
            throw new OrderBusinessException(MessageConstant.ORDER_NOT_FOUND);
        }
        //2.判断订单的当前状态,若在已接单之前,可直接取消订单,并且退款
        //订单状态 1待付款 2待接单 3已接单 4派送中 5已完成 6已取消
        if (ordersDB.getStatus() > 2) {
            //订单状态>2说明 订单已经被接单,这时候不能直接退款
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        Orders orders = new Orders();
        orders.setId(ordersDB.getId());
        //3.待接单状态,直接退款,进行退款操作
        /**  refund 四个参数
         * outTradeNo    商户订单号
         * outRefundNo   商户退款单号
         * refund        退款金额
         * total         原订单金额
         */
        if (ordersDB.getStatus().equals(TO_BE_CONFIRMED)) {
            weChatPayUtil.refund(ordersDB.getNumber(), ordersDB.getNumber(), new BigDecimal(0.01), new BigDecimal(0.01));
            orders.setStatus(Orders.REFUND);
        }

        //4.更新订单状态、取消原因、取消时间
        orders.setStatus(CANCELLED);  //订单状态
        orders.setCancelReason("用户取消");  //取消原因
        orders.setCancelTime(LocalDateTime.now());  //取消时间
        //保存修改
        orderMapper.update(orders);
    }

    /**
     * 再来一单
     *
     * @param id
     */
    @Override
    public void anotherOrder(Long id) {
        //1.根据订单id获取订单详情
        Orders orders = orderMapper.findById(id);
        //2.获取里面有哪些菜品和套餐
        List<OrderDetail> orderDetails = orderDetailMapper.getByOrderId(id);

        //3.把订单详情数据复制到购物车 实现 添加到购物车
        Long userId = BaseContext.getCurrentId();  //获取用户id
        ShoppingCart shoppingCart = new ShoppingCart();  //创建购物车对象
        for (OrderDetail orderDetail : orderDetails) {  //遍历
            BeanUtils.copyProperties(orderDetail, shoppingCart);  //将订单详情对象 转换为 购物车对象
        }
        shoppingCart.setUserId(userId);  //设置userId
        shoppingCart.setCreateTime(LocalDateTime.now());  //设置 创建时间
        //最后将购物车数据存储到数据库中
        shoppingCartMapper.save(shoppingCart);
    }

    /**
     * 订单搜索
     *
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult conditionSearch(OrdersPageQueryDTO ordersPageQueryDTO) {
        // 使用PageHelper开始分页
        PageHelper.startPage(ordersPageQueryDTO.getPage(), ordersPageQueryDTO.getPageSize());
        // 使用orderMapper执行分页查询
        Page<Orders> page = orderMapper.page(ordersPageQueryDTO);
        // 部分订单状态，需要额外返回订单菜品信息，将Orders转化为OrderVO
        List<OrderVO> orderVOList = getOrderVOList(page);
        // 将最终结果作为PageResult对象返回，其中包含总数和订单列表
        return new PageResult(page.getTotal(), orderVOList);
    }

    /**
     * 辅助方法:getOrderVOList，用于将 Orders 对象转换为 OrderVO 对象，并添加订单菜品信息
     *
     * @param page
     * @return
     */
    private List<OrderVO> getOrderVOList(Page<Orders> page) {
        // 需要返回订单菜品信息，自定义OrderVO响应结果
        List<OrderVO> orderVOList = new ArrayList<>();
        //通过 page.getResult() 获取 Page<Orders> 对象中的订单列表
        List<Orders> ordersList = page.getResult();
        if (!CollectionUtils.isEmpty(ordersList)) { //如果订单列表不为空,遍历
            for (Orders orders : ordersList) {
                //将 Orders 对象的共同字段复制到 OrderVO 对象中
                OrderVO orderVO = new OrderVO();
                BeanUtils.copyProperties(orders, orderVO);
                //获取订单菜品信息的字符串 representation
                String orderDishes = getOrderDishesStr(orders);

                // 将订单菜品信息封装到orderVO中，并添加到orderVOList
                orderVO.setOrderDishes(orderDishes);
                orderVOList.add(orderVO);
            }
        }
        return orderVOList;
    }

    /**
     * 根据订单id获取菜品信息字符串
     *
     * @param orders
     * @return
     */
    private String getOrderDishesStr(Orders orders) {
        // 查询订单菜品详情信息（订单中的菜品和数量）
        List<OrderDetail> orderDetailList = orderDetailMapper.getByOrderId(orders.getId());

        // 将每一条订单菜品信息拼接为字符串（格式：宫保鸡丁*3；）
        List<String> orderDishList = orderDetailList.stream().map(x -> {
            String orderDish = x.getName() + "*" + x.getNumber() + ";";
            return orderDish;
        }).collect(Collectors.toList());

        // 将该订单对应的所有菜品信息拼接在一起
        return String.join("", orderDishList);
    }

    /**
     * 取消订单
     * @param ordersCancelDTO
     */
    /**
     * 取消订单
     *
     * @param ordersCancelDTO
     */
    public void cancel(OrdersCancelDTO ordersCancelDTO) throws Exception {
        // 根据id查询订单
        Orders ordersDB = orderMapper.findById(ordersCancelDTO.getId());

        //支付状态
        Integer payStatus = ordersDB.getPayStatus();
        if (payStatus == 1) {
            //用户已支付，需要退款
            String refund = weChatPayUtil.refund(
                    ordersDB.getNumber(),
                    ordersDB.getNumber(),
                    new BigDecimal(0.01),
                    new BigDecimal(0.01));
        }

        // 管理端取消订单需要退款，根据订单id更新订单状态、取消原因、取消时间
        Orders orders = new Orders();
        orders.setId(ordersCancelDTO.getId());
        orders.setStatus(CANCELLED);
        orders.setCancelReason(ordersCancelDTO.getCancelReason());
        orders.setCancelTime(LocalDateTime.now());
        orderMapper.update(orders);
    }

    /**
     * 检查客户的收货地址是否超出配送范围
     *
     * @param address
     */
    private void checkOutOfRange(String address) {
        Map map = new HashMap();
        map.put("address", shopAddress);
        map.put("output", "json");
        map.put("ak", ak);

        //获取店铺的经纬度坐标
        String shopCoordinate = HttpClientUtil.doGet("https://api.map.baidu.com/geocoding/v3", map);

        JSONObject jsonObject = JSON.parseObject(shopCoordinate);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("店铺地址解析失败");
        }

        //数据解析
        JSONObject location = jsonObject.getJSONObject("result").getJSONObject("location");
        String lat = location.getString("lat");
        String lng = location.getString("lng");
        //店铺经纬度坐标
        String shopLngLat = lat + "," + lng;

        map.put("address", address);
        //获取用户收货地址的经纬度坐标
        String userCoordinate = HttpClientUtil.doGet("https://api.map.baidu.com/geocoding/v3", map);

        jsonObject = JSON.parseObject(userCoordinate);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("收货地址解析失败");
        }

        //数据解析
        location = jsonObject.getJSONObject("result").getJSONObject("location");
        lat = location.getString("lat");
        lng = location.getString("lng");
        //用户收货地址经纬度坐标
        String userLngLat = lat + "," + lng;

        map.put("origin", shopLngLat);
        map.put("destination", userLngLat);
        map.put("steps_info", "0");

        //路线规划
        String json = HttpClientUtil.doGet("https://api.map.baidu.com/directionlite/v1/driving", map);

        jsonObject = JSON.parseObject(json);
        if (!jsonObject.getString("status").equals("0")) {
            throw new OrderBusinessException("配送路线规划失败");
        }

        //数据解析
        JSONObject result = jsonObject.getJSONObject("result");
        JSONArray jsonArray = (JSONArray) result.get("routes");
        Integer distance = (Integer) ((JSONObject) jsonArray.get(0)).get("distance");

        if (distance > 5000) {
            //配送距离超过5000米
            throw new OrderBusinessException("超出配送范围");
        }
    }

    /**
     * 派送订单
     *
     * @param id
     */
    @Override
    public void deliveryOrder(Long id) {
        //根据id查询订单
        Orders ordersDB = orderMapper.findById(id);
        //判断订单是否存在,并且状态是3
        if (ordersDB == null || !ordersDB.getStatus().equals(Orders.CONFIRMED)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        Orders orders = new Orders();
        orders.setId(ordersDB.getId());
        // 更新订单状态,状态转为派送中
        orders.setStatus(Orders.DELIVERY_IN_PROGRESS);

        orderMapper.update(orders);
    }

    /**
     * 完成订单
     *
     * @param id
     */
    @Override
    public void completeOrder(Long id) {
        //根据id查询订单
        Orders ordersDB = orderMapper.findById(id);
        //判断订单是否存在,并且状态是4
        if (ordersDB == null || !ordersDB.getStatus().equals(Orders.DELIVERY_IN_PROGRESS)) {
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        Orders orders = new Orders();
        orders.setId(ordersDB.getId());
        // 更新订单状态,状态转为派送中
        orders.setStatus(Orders.COMPLETED);
        orders.setDeliveryTime(LocalDateTime.now());
        orderMapper.update(orders);
    }

    /**
     * 查询所有的订单状态数量信息
     *
     * @return
     */
    @Override
    public OrderStatisticsVO findAllByStatus() {
        //待接单数量     待派送数量    派送中数量
        OrderStatisticsVO orderStatisticsVO = new OrderStatisticsVO();
        //待接单数量
        Integer statusSum = orderMapper.findAllByStatus(TO_BE_CONFIRMED);
        //待派送数量
        Integer statusSumConfirmed = orderMapper.findAllByStatus(Orders.CONFIRMED);
        //派送中数量
        Integer statusSumProgress = orderMapper.findAllByStatus(Orders.DELIVERY_IN_PROGRESS);
        //待接单数量
        orderStatisticsVO.setToBeConfirmed(statusSum);
        //待派送数量
        orderStatisticsVO.setConfirmed(statusSumConfirmed);
        //派送中数量
        orderStatisticsVO.setDeliveryInProgress(statusSumProgress);
        return orderStatisticsVO;
    }

    /**
     * 接单
     *
     * @param ordersConfirmDTO
     */
    @Override
    public void confirm(OrdersConfirmDTO ordersConfirmDTO) {
        //根据id查询数据库中是否存在订单
        Orders orders = orderMapper.findById(ordersConfirmDTO.getId());
        //将传入的数据复制到orders中
//        BeanUtils.copyProperties(ordersConfirmDTO,orders);
        orders.setStatus(ordersConfirmDTO.getStatus());
        //将新的订单状态添加到orders
        orders.setStatus(Orders.CONFIRMED);
        //修改
        orderMapper.update(orders);
    }

    /**
     * 拒单
     *
     * @param ordersRejectionDTO
     */
    @Override
    public void rejection(OrdersRejectionDTO ordersRejectionDTO) {
        //根据id查询数据库中是否存在订单
        Orders orders = orderMapper.findById(ordersRejectionDTO.getId());
        //修改状态
        orders.setStatus(CANCELLED);
        //将取消原因添加
        orders.setRejectionReason(ordersRejectionDTO.getRejectionReason());
        //修改
        orderMapper.update(orders);
    }

    /**
     *
     * @param ordersPaymentDTO
     */
    @Override
    public void payment(OrdersPaymentDTO ordersPaymentDTO) {
        //更新订单状态
        Orders orders = new Orders();
        orders.setPayStatus(1);  //1已支付
        orders.setPayMethod(ordersPaymentDTO.getPayMethod());
        orders.setNumber(ordersPaymentDTO.getOrderNumber());  //订单号
        orders.setStatus(TO_BE_CONFIRMED);  //2待接单
        orderMapper.updateStatus(orders);


        Map map = new HashMap();
        map.put("type", 1);  //type 为消息类型，1为来单提醒  2为客户催单
        map.put("orderId", orders.getId());  //orderld 为订单id
        map.put("content", "订单号" + orders.getNumber());  //content 为消息内容
        String json = JSON.toJSONString(map);
        //将该 JSON 字符串发送给所有的客户端
        webSocketServer.sendToAllClient(json);
    }

    /**
     * 催单
     * @param id
     */
    @Override
    public void reminder(Long id) {
        //根据订单id查询订单信息
        Orders orderDB = orderMapper.findById(id);
        if (orderDB == null){
            throw new OrderBusinessException(MessageConstant.ORDER_STATUS_ERROR);
        }
        Map map = new HashMap();
        map.put("type", 2);  //type 为消息类型，1为来单提醒  2为客户催单
        map.put("orderId", id);  //orderld 为订单id
        map.put("content", "订单号" + orderDB.getNumber());  //content 为消息内容
        String json = JSON.toJSONString(map);
        //将该 JSON 字符串发送给所有的客户端
        webSocketServer.sendToAllClient(json);
    }
}

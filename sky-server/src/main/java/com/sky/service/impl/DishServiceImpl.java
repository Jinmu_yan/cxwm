package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 演示：
 */
@Service
public class DishServiceImpl implements DishService {
    @Autowired
    DishMapper dishMapper;

    @Autowired
    DishFlavorMapper dishFlavorMapper;

    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    SetmealDishMapper setmealDishMapper;

    @Autowired
    SetmealMapper setmealMapper;
    /**
     * 添加菜品
     */
    @Transactional  //用到多表时,开启声明式事物
    @Override
    public void save(DishDTO dishDTO) {
        //1.保存菜品
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        //这里设置了主键返回
        dishMapper.save(dish);
        //2.保存口味信息
        //注意:这里需要给每个口味对象设置dishId
        List<DishFlavor> flavorList = dishDTO.getFlavors().stream()
                .map(dishFlavor -> {
                    //拿到每个口味信息,并设置id
                    dishFlavor.setDishId(dish.getId());
                    return dishFlavor;
                }).collect(Collectors.toList());
        //这里用的是dishFlavorMapper 调方法
        dishFlavorMapper.saveDishFlavor(flavorList);

        //清理缓存
        String CACHE_KEY = "DISH_"+dishDTO.getCategoryId();
        redisTemplate.delete(CACHE_KEY);
    }

    /**
     * 菜品分页查询
     * @param pageQueryDTO
     * @return
     */
    @Override
    public PageResult page(DishPageQueryDTO pageQueryDTO) {
        PageHelper.startPage(pageQueryDTO.getPage(),pageQueryDTO.getPageSize());
        //Dish和页面对不上,少一个分类名称
        Page<Dish> page = dishMapper.page(pageQueryDTO);
        List<DishVO> list = page.getResult().stream()
                .map(dish -> {
                    DishVO dishVO = new DishVO();
                    BeanUtils.copyProperties(dish, dishVO);
                    //根据 category_id 查询分类信息
                    Category category = categoryMapper.findById(dish.getCategoryId());
                    dishVO.setCategoryName(category.getName());
                    return dishVO;
                }).collect(Collectors.toList());
        return new PageResult(page.getTotal(),list);
    }

    /**
     * 批量删除菜品
     * @param ids
     */
    @Transactional
    @Override
    public void delByIds(List<Long> ids) {
        //1.菜品起售不能删除
        ids.forEach(id->{
            //判断当前菜品状态是否为1
            Dish dish = dishMapper.getById(id);//后绪步骤实现
            if (dish.getStatus() == StatusConstant.ENABLE) {
                //当前菜品处于起售中，不能删除
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        });
        //判断是否关联套餐
        List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishIds(ids);
        if (setmealIds != null && setmealIds.size() > 0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL); //被关联
        }
        //删除菜品数据
        dishMapper.deleteByIds(ids);
        //删除菜品关联的口味数据
        dishFlavorMapper.deleteByDishIds(ids);

        //清理缓存
        //现在这里拿到的ids是菜品的id 构造的CACHE_KEY 是根据分类id来构造的
        //这里直接把所有DISH_分类id的数据全删除
        //获取所有key
        Set keys = redisTemplate.keys("DISH_*");
        redisTemplate.delete(keys);
    }

    /**
     * 根据id查询菜品以及口味信息
     * @param id
     * @return
     */
    @Override
    public DishVO getByIdWithFlavor(Long id) {
        //根据id查询菜品
        Dish dish = dishMapper.getById(id);
        //根据id查询口味
        List<DishFlavor> dishFlavors =dishFlavorMapper.getByDishId(id);
        //将查询到的数据封装到DishVO
        DishVO dishVO = new DishVO();
        BeanUtils.copyProperties(dish, dishVO);
        dishVO.setFlavors(dishFlavors);
        return dishVO;
    }

    /**
     * 根id修改菜品基本信息和对应的口味信息
     * @param dishDTO
     */
    @Transactional
    @Override
    public void updateWithFlavor(DishDTO dishDTO) {
        //1.修改菜品的基本信息
          //修改基本信息时,无需修改菜品,所以用dish表比较合适
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.update(dish);
        //2.删除菜品原有的口味
        dishFlavorMapper.deleteByDishId(dishDTO.getId());
        //3.再添加新的口味
        List<DishFlavor> flavorList = dishDTO.getFlavors();
        if (flavorList != null && flavorList.size() > 0){  //查看是否有口味数据
            flavorList.forEach(dishFlavor -> {  //遍历口味数据
                dishFlavor.setDishId(dishDTO.getId());  //修改口味数据
            });
            //向口味表插入n条数据
            dishFlavorMapper.saveDishFlavor(flavorList);  //调用添加菜品中的saveDishFlavor
        }
    }

    /**
     * 菜品启售停售
     * @param status
     * @param id
     */
    @Override
    public void startOrStop(Integer status, Long id) {
        Dish dish = Dish.builder()
                .id(id)
                .status(status)
                .build();
        dishMapper.update(dish);

        //如果停售,还需要将包含当前菜品的套餐也停售
        List<Long> dishIds = new ArrayList<>();
        dishIds.add(id);
        List<Long> setmealIds = setmealDishMapper.getSetmealIdsByDishIds(dishIds);
        if (setmealIds != null && setmealIds.size() > 0) {
            for (Long setmealId : setmealIds) {
                Setmeal setmeal = Setmeal.builder()
                        .id(setmealId)
                        .status(StatusConstant.DISABLE)
                        .build();
                setmealMapper.update(setmeal);
            }
        }
    }

    /**
     * 套餐管理-新增套餐-添加菜品-菜品分类(补充)
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> findListById(Long categoryId) {
        Dish dish = Dish.builder()
                .categoryId(categoryId)
                .status(StatusConstant.ENABLE)
                .build();
        List<Dish> list = dishMapper.list(dish);
        return list;
    }

    @Autowired
    RedisTemplate redisTemplate;
    /**
     * 条件查询菜品和口味
     * @return
     */
    public List<DishVO> listWithFlavor(DishDTO dishDTO) {
        String CACHE_KEY = "DISH_" + dishDTO.getCategoryId();
        //1.查缓存
        List<DishVO> dishVOList = (List<DishVO>) redisTemplate.opsForValue().get(CACHE_KEY);
        //2.判断缓存是否为null
        if (dishVOList != null){
            return dishVOList;
        }

        //如果为null,进数据库查
        List<Dish> dishList = dishMapper.findDishesByCid(dishDTO);
        dishVOList = new ArrayList<>();

        for (Dish d : dishList) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d,dishVO);

            //根据菜品id查询对应的口味
            List<DishFlavor> flavors = dishFlavorMapper.findByDishId(d.getId());

            dishVO.setFlavors(flavors);
            dishVOList.add(dishVO);
        }

        //3.存缓存
        redisTemplate.opsForValue().set(CACHE_KEY,dishVOList);
        return dishVOList;
    }
}


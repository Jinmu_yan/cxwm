package com.sky.test;

import com.sky.SkyApplication;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import sun.net.www.http.HttpClient;

import java.io.IOException;

/**
 * 演示：
 */
@SpringBootTest(classes = SkyApplication.class)
public class HttoClientTest {

    /**
     * HttpClient发送get请求
     */
    @Test
    public void testGet() throws IOException {
        //创建httpClient对象
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //创建请求对象
        HttpGet httpGet = new HttpGet("http://localhost:8080/user/shop/status");
        //发送请求,并接收响应结果
        CloseableHttpResponse response = httpClient.execute(httpGet);

        //获取服务器返回的状态码
        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("服务端返回的状态码为:"+statusCode);
        //获取响应数据
        HttpEntity entity = response.getEntity();  //获取响应体
        //通过工具类解析对象
        String body = EntityUtils.toString(entity);  //解析为string字符串
        System.out.println("服务端返回的数据为:"+body);

        //关闭资源
        response.close();
        httpClient.close();
    }
}

package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {
    /**
     * 存储订单表数据
     *
     * @param orders
     */
    void save(Orders orders);

    /**
     * 修改订单状态
     *
     * @param orders
     */
    void update(Orders orders);

    /**
     * 更新订单状态
     * @param orders
     */
    void updateStatus(Orders orders);

    /**
     * 根据订单号查询订单
     *
     * @param orderNum
     * @return
     */
    Orders findByOrderNum(String orderNum);

    /**
     * 进行分页条件查询
     *
     * @param ordersPageQueryDTO
     * @return
     */
    Page<Orders> page(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 根据id查询订单详情
     *
     * @param id
     * @return
     */
    Orders findById(Long id);

    /**
     * 查询所有的订单状态数量信息
     *
     * @param toBeConfirmed
     * @return
     */
    Integer findAllByStatus(Integer toBeConfirmed);

    /**
     * 根据订单状态和下单时间查询订单
     *
     * @param status
     * @param orderTime
     * @return
     */
    //@Select("select * from orders where status = #{status} and order_time < #{orderTime}")
    List<Orders> getTime(Integer status, LocalDateTime orderTime);

    /**
     * 统计指定时间内的营业额
     * @param map
     * @return
     */
    Double sumByMap(Map map);

    /**
     * 根据条件统计订单数量
     * @param map
     * @return
     */
    Integer countByMap(Map map);

    /**
     * 统计指定时间区间内的销量排名前10
     * @param beginTime
     * @param endTime
     * @return
     */
    List<GoodsSalesDTO> getSalesTop10(LocalDateTime beginTime, LocalDateTime endTime);


}

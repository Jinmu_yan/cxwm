package com.sky.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sky.constant.MessageConstant;
import com.sky.constant.WXConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.LoginFailedException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 演示：
 */
@Service
public class UserServiceImpl implements UserService {
    //微信服务接口地址
    public static final String URL = "https://api.weixin.qq.com/sns/jscode2session";

    @Autowired
    WeChatProperties weChatProperties;

    @Autowired
    UserMapper userMapper;

    /**
     * 用户端-微信登录
     * @param userLoginDTO
     * @return
     */
    @Override
    public User wxLogin(UserLoginDTO userLoginDTO) throws JsonProcessingException {
        /**
         * 步骤:
         * 1.小程序点击允许,发送登录请求,根据小程序传过来的授权码,去获取用户的唯一标识openid
         * 2.判断openid是否为null,为null标识出错或超时(五分钟)抛异常,不为null表示成功
         * 3.再根据openid去数据库中查找user用户,看是否已经登陆过
         * 4.查询后,若user=null,表示user用户第一次访问,接下来进行注册,并保存在数据库中
         * 5.查询后,若user存在,则说明已经登陆过,直接登录并返回user对象
         * 6.server层接收到返回的user对象后,生成jwt令牌token
         * 7.最终将id,openid,token返回给小程序
         */
        //1.小程序点击允许,发送登录请求,根据小程序传过来的授权码,去获取用户的唯一标识openid
        String openId = getOpenId(userLoginDTO.getCode());
        System.out.println(openId);
        //2.判断openid是否为null,为null标识出错或超时(五分钟)抛异常,不为null表示成功
        if (openId == null){
            throw new LoginFailedException(MessageConstant.LOGIN_FAILED);
        }
        //3.再根据openid去数据库中查找user用户,看是否已经登陆过
        User user = userMapper.findUserByOpenid(openId);
        //4.查询后,若user=null,表示user用户第一次访问,接下来进行注册,并保存在数据库中
        if (user == null){
            //注册
            user = User.builder()
                    .openid(openId)
                    .createTime(LocalDateTime.now())
                    .build();
            //保存在数据库中
            userMapper.save(user);
        }
        return user;
    }

    /**
     * 根据授权码获取openid
     * @param authCode
     * @return
     */
    //通过发送HTTP的GET请求来获取用户的OpenID，并将其返回
    private String getOpenId(String authCode) throws JsonProcessingException {
        //创建集合,存储请求参数
        Map<String, String> param = new HashMap<String, String>();
        param.put(WXConstant.APP_ID,weChatProperties.getAppid());
        param.put(WXConstant.APP_SECRET,weChatProperties.getSecret());
        param.put(WXConstant.JS_CODE,authCode);
        param.put(WXConstant.GRANT_TYPE,"authorization_code");
        String jsonResult = HttpClientUtil.doGet(URL, param);
        //创建ObjectMapper对象,将json字符串解析为java对象
        ObjectMapper objectMapper = new ObjectMapper();
        //调用readValue方法,将jsonResult解析为map对象
        Map<String, String> map = objectMapper.readValue(jsonResult, Map.class);
        //从解析后的map中获取openid对应的值
        return map.get(WXConstant.OPEN_ID);
    }
}

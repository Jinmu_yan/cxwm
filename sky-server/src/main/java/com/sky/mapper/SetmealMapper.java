package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.TableInject;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    /**
     * 根据分类id查询套餐的数量
     * @param id
     * @return
     */
    Integer countByCategoryId(Long id);

    /**
     * 菜品启售停售
     * @param setmeal
     */
    @TableInject(OperationType.UPDATE)   //自动填充
    void update(Setmeal setmeal);

    /**
     * 添加套餐
     * @param setmeal
     */
    @TableInject(OperationType.INSERT)   //自动填充
    void addSetmeal(Setmeal setmeal);

    /**
     * 保存套餐和菜品的关联关系
     * @param setmealDishes
     */
    Integer addSetmealDish(List<SetmealDish> setmealDishes);

    /**
     * 分页查询
     * @param pageQueryDTO
     * @return
     */
    Page<SetmealVO> page(SetmealPageQueryDTO pageQueryDTO);

    /**
     * 根据id查询套餐信息
     * @param id
     * @return
     */
    Setmeal getById(Long id);

    /**
     * 删除套餐基本数据
     * @param ids
     */
    void deleteByIds(List<Long> ids);

    /**
     * 删除套餐关联的菜品数据
     * @param ids
     */
    void deleteBySetmealIds(List<Long> ids);

    /**
     *更新套餐信息
     * @param setmeal
     */
    @TableInject(OperationType.UPDATE)   //自动填充
    void updateSetmeal(Setmeal setmeal);

    /**
     * 删除套餐和菜品的关联
     * @param setmealId
     */
    void deleteBySetmealId(Long setmealId);

    /**
     * 重新插入套餐和菜品的关系
     * @param setmealDishList
     */
    void saveSetmealDish(List<SetmealDish> setmealDishList);

    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    SetmealVO findById(Long id);

    List<Setmeal> list(SetmealDTO setmealDTO);
}

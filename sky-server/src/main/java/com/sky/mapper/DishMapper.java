package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.TableInject;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    Integer countByCategoryId(Long categoryId);

    /**
     * 添加菜品
     * @param dish
     */
    @TableInject(OperationType.INSERT)
    void save(Dish dish);

    /**
     * 菜品分页查询
     * @param pageQueryDTO
     * @return
     */
    Page<Dish> page(DishPageQueryDTO pageQueryDTO);

    /**
     * 批量删除菜品
     * @param ids
     */
    void delByIds(List<Long> ids);

    /**
     * 根据id查询套餐数量
     * @param id
     * @return
     */
    Integer countBySetmealId(Long id);

    /**
     * 根据id删除菜品数据
     * @param id
     */
    //void deleteByIds(Long id);
    void deleteByIds(List<Long> ids);

    /**
     * 根据id查询菜品以及口味信息
     * @param id
     * @return
     */
    Dish getById(Long id);

    /**
     *修改菜品的基本信息
     * @param dish
     */
    @TableInject(OperationType.UPDATE)   //自动填充
    void update(Dish dish);

    /**
     * 套餐管理-新增套餐-添加菜品-菜品分类(补充)
     * @param dish
     * @return
     */
    List<Dish> list(Dish dish);

    /**
     *根据id获取包含这个套餐的菜品
     * @param id
     * @return
     */
    //List<Dish> getBySetmealId(Long id);

    /**
     * 获取套餐中的菜品id
     * @param dishId
     * @return
     */
    Dish findById(Long dishId);

    /**
     * 根据分类id查询菜品列表
     * @param dishDTO
     * @return
     */
    List<Dish> findDishesByCid(DishDTO dishDTO);

    /**
     * 根据菜品id查询菜品
     * @param dishId
     * @return
     */
    Dish findByDishId(Long dishId);
}

package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingCartMapper;
import com.sky.service.ShoppingCartService;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 演示：
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired
    ShoppingCartMapper shoppingCartMapper;

    @Autowired
    DishMapper dishMapper;

    @Autowired
    SetmealMapper setmealMapper;

    /**
     *添加购物车
     * @param shoppingCartDTO
     */
    /*@Override
    public void add(ShoppingCartDTO shoppingCartDTO) {
        //1.根据userId和参数,查询当前商品之前是否添加过购物车
        Long userId = BaseContext.getCurrentId();  //获取当前用户的用户ID
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);  //将Dto的值 复制到 shoppingCart
        shoppingCart.setUserId(userId);  //将userId 设置到shoppingCart对象的userId中,实现用户id与购物车关联
        ShoppingCart shoppingCartDB = shoppingCartMapper.findShoppingCartOptions(shoppingCart);  //购物车查询

        //2.添加过,则商品数量+1
        if (shoppingCartDB != null){
            shoppingCartDB.setNumber(shoppingCartDB.getNumber()+1);
            //更新商品数量
            shoppingCartMapper.updateNumberById(shoppingCartDB);

        //3.没添加过,则添加商品(商品分为两种,菜品和套餐)
        }else {
            Long dishId = shoppingCartDTO.getDishId();
            //3.1 添加菜品
            if (dishId != null){
                Dish dish = dishMapper.findByDishId(dishId);  //根据菜品id查询菜品信息
                //需要添加到购物车表中的属性
                shoppingCart.setName(dish.getName());  //菜品名称
                shoppingCart.setImage(dish.getImage());  //菜品图片
                shoppingCart.setAmount(dish.getPrice());  //菜品价格
            }else {
            //3.2 添加套餐

                //根据套餐id查询套餐信息
                *//*SetmealVO setmeal = setmealMapper.findById(shoppingCartDTO.getSetmealId());
                //需要添加到购物车表中的属性
                shoppingCart.setName(setmeal.getName());  //菜品名称
                shoppingCart.setImage(setmeal.getImage());  //菜品图片
                shoppingCart.setAmount(setmeal.getPrice());  //菜品价格*//*
            }
            shoppingCart.setNumber(1);  //数量
            shoppingCart.setCreateTime(LocalDateTime.now());
        }

        //4.将购物车数据存入数据库
        shoppingCartMapper.save(shoppingCart);
    }*/
    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {
        //1.根据用户id和参数查询 当前用户之前有没有把对应商品加入购物车
        Long userId = BaseContext.getCurrentId();
        ShoppingCart shoppingCart = new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);
        shoppingCart.setUserId(userId);
        ShoppingCart shoppingCartDB = shoppingCartMapper.findShoppingCartOptions(shoppingCart);

        if(shoppingCartDB != null){
            //2.添加过,则商品数量+1
            shoppingCartDB.setNumber(shoppingCartDB.getNumber() + 1);
            shoppingCartMapper.updateNumberById(shoppingCartDB);
        }else{
            Long dishId = shoppingCartDTO.getDishId();
            //3.没添加过,则添加商品(商品分为两种,菜品和套餐)
            if(dishId != null){
                //3.1 添加菜品
                //根据菜品id查询菜品
                Dish dish = dishMapper.findByDishId(dishId);  //根据菜品id查询菜品信息
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());
            }else{
                //3.2 添加套餐
                //根据套餐id查询套餐
                SetmealVO setmeal = setmealMapper.findById(shoppingCartDTO.getSetmealId());
                //需要添加到购物车表中的属性
                shoppingCart.setName(setmeal.getName());  //菜品名称
                shoppingCart.setImage(setmeal.getImage());  //菜品图片
                shoppingCart.setAmount(setmeal.getPrice());  //菜品价格
            }
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());

            //存入数据库
            shoppingCartMapper.save(shoppingCart);
        }
    }
    /**
     * 查看购物车
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
        //1.获取用户id
        //2.根据用户id查询购物车表
        return shoppingCartMapper.findByUserId(BaseContext.getCurrentId());
    }

    /**
     * 清空购物车
     */
    @Override
    public void cleanShoppingCart() {
        //获取用户id,然后传入删除
        Long userId = BaseContext.getCurrentId();
        shoppingCartMapper.deleteById(userId);
    }

    /**
     * 删除购物车一个商品
     * @param shoppingCartDTO
     */
    @Override
    public void subShoppingCart(ShoppingCartDTO shoppingCartDTO) {
        //1.获取用户/菜品/套餐的id
        Long userId = BaseContext.getCurrentId();
        Long dishId = shoppingCartDTO.getDishId();
        Long setmealId = shoppingCartDTO.getSetmealId();
        //使用构建器模式创建一个 ShoppingCart 对象 condition，包含用户 ID、菜品 ID、套餐 ID 和菜品口味。
        ShoppingCart condition = ShoppingCart.builder()
                .userId(userId)
                .dishId(dishId)
                .setmealId(setmealId)
                .dishFlavor(shoppingCartDTO.getDishFlavor())
                .build();
        //通过条件对象 condition 查询符合条件的购物车列表。返回的结果存储在 shoppingCartList 中
        List<ShoppingCart> shoppingCartList = shoppingCartMapper.findShoppingCartByCondiotion(condition);
        ShoppingCart shoppingCart = shoppingCartList.get(0);  //获取第一个购物车对象
        //获取购物车的 ID 和数量
        Long id = shoppingCart.getId();
        Integer number = shoppingCart.getNumber();
        //根据购物车商品数量的情况进行判断
        if (number > 1 ) {
            shoppingCartMapper.updateNumber(shoppingCart.getId(),-1);  //更新商品数量 -1
        }else {
            shoppingCartMapper.deleteShoppingCart(shoppingCart.getId());  //根据id删除商品
        }
    }

}

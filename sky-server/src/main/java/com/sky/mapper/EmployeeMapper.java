package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.anno.TableInject;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    Employee getByUsername(String username);

    /**
     * 添加员工
     * @param employee
     */
    @TableInject(OperationType.INSERT)
    void save(Employee employee);

    /**
     * 员工分页查询
     * @param name
     * @return
     */
    Page page(String name);

    /**
     * 启用禁用员工
     * @param employee
     * @return
     */
    @TableInject(OperationType.UPDATE)
    void update(Employee employee);

    /**
     * 根据id查询员工信息
     * @param id
     * @return
     */
    Employee findById(Long id);

}

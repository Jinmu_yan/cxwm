package com.sky.controller;

import com.sky.service.OrderService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 演示：
 */
@RestController
@RequestMapping("/notify")
@Api(tags = "微信支付相关接口")
public class NotifyController {

    @Autowired
    OrderService orderService;

    /**
     * 当小程序支付成功后,微信访问的接口
     * 微信告诉后台,哪个订单支付成功了
     */
    @RequestMapping("/paySuccess")
    public String paySuccess(String orderNum){
        //支付成功
        orderService.update(orderNum);
        return "ok";
    }
}

package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {
    /**
     *添加菜品
     * @param dishDTO
     */
    void save(DishDTO dishDTO);

    /**
     * 菜品分页查询
     * @param pageQueryDTO
     * @return
     */
    PageResult page(DishPageQueryDTO pageQueryDTO);

    /**
     * 批量删除菜品
     * @param ids
     */
    void delByIds(List<Long> ids);

    /**
     * 根据id查询菜品以及口味信息
     * @param id
     * @return
     */
    DishVO getByIdWithFlavor(Long id);

    /**
     * 根id修改菜品基本信息和对应的口味信息
     * @param dishDTO
     */
    void updateWithFlavor(DishDTO dishDTO);

    /**
     * 菜品启售停售
     * @param status
     * @param id
     */
    void startOrStop(Integer status, Long id);

    /**
     * 套餐管理-新增套餐-添加菜品-菜品分类(补充)
     * @param categoryId
     * @return
     */
    List<Dish> findListById(Long categoryId);

    /**
     * 查询菜品信息(封装口味)
     * @param dishDTO
     * @return
     */
    List<DishVO> listWithFlavor(DishDTO dishDTO);
}

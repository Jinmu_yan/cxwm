package com.sky.controller.user;

import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user/setmeal")
@Api(tags = "C端-套餐浏览接口")
public class SetmealUserController {
    @Autowired
    private SetmealService setmealService;
    @Autowired
    SetmealDishMapper setmealDishMapper;
    @Autowired
    DishMapper dishMapper;
    /**
     * 条件查询
     *
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    @ApiOperation("根据分类id查询套餐")
    public Result<List<Setmeal>> list(Long categoryId) {
        SetmealDTO setmealDTO = new SetmealDTO();
        setmealDTO.setCategoryId(categoryId);
        setmealDTO.setStatus(StatusConstant.ENABLE);

        List<Setmeal> list = setmealService.list(setmealDTO);
        return Result.success(list);
    }

    /**
     * 根据套餐id查询包含的菜品列表
     *
     * @param id
     * @return
     */
    @GetMapping("/dish/{id}")
    @ApiOperation("根据套餐id查询包含的菜品列表")
    public Result<List<DishItemVO>> dishList(@PathVariable("id") Long id) {
        List<DishItemVO> list = setmealService.getDishItemById(id);
        return Result.success(list);
    }

    /**
     * 根据id查询菜品选项
     * @param id
     * @return
     */
    public List<DishItemVO> getDishItemById(Long id) {
        //根据套餐id查询绑定的所有菜品关系
        List<SetmealDish> setmealDishes = setmealDishMapper.findBySetmealId(id);

        List<DishItemVO> dishItemList = setmealDishes.stream()
                .map(setmealDish -> {
                    Long dishId = setmealDish.getDishId();
                    //根据菜品id查询菜品信息
                    Dish dish = dishMapper.findById(dishId);
                    //封装DishItem
                    DishItemVO dishItemVO = new DishItemVO();
                    dishItemVO.setImage(dish.getImage());
                    dishItemVO.setDescription(dish.getDescription());
                    dishItemVO.setCopies(setmealDish.getCopies());
                    dishItemVO.setName(setmealDish.getName());
                    return dishItemVO;
                }).collect(Collectors.toList());

        return dishItemList;
    }
}
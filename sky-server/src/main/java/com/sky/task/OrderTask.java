package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 演示：定时任务
 */
@Component
@Slf4j
public class OrderTask {
    @Autowired
    OrderMapper orderMapper;

    /**
     * 处理待支付超时订单
     */
    //@Scheduled(cron = "0 * * * * ?")  //每隔一分钟触发一次
    //@Scheduled(cron = "1/5 * * * * ?")  //每隔5秒触发一次
    public void executeTask(){
        log.info("定时处理超时订单:{}", LocalDateTime.now());
        //当前时间-15 = 下单时间
        LocalDateTime time = LocalDateTime.now().minusMinutes(15);
        //传入订单状态和下单时间
        List<Orders> orderList = orderMapper.getTime(Orders.PENDING_PAYMENT, time);
        //遍历订单集合,修改状态,取消原因,取消时间
        if (orderList != null && orderList.size() > 0){
            for (Orders orders : orderList) {
                orders.setStatus(Orders.CANCELLED);  //状态改为 已取消
                orders.setCancelReason("超时未支付,自动取消");  //取消原因
                orders.setCancelTime(LocalDateTime.now());  //取消时间
                orderMapper.update(orders);
            }
        }
    }
    //订单状态 1待付款 2待接单 3已接单 4派送中 5已完成 6已取消
    //支付状态 0未支付 1已支付 2退款
    /**
     * 处理一直处于派送中的订单
     */
    //@Scheduled(cron = "0 0 1 * * ?")  //每天凌晨一点执行
    //@Scheduled(cron = "0/5 * * * * ?")  //每隔5秒触发一次
    public void Task2(){
        log.info("定时处理派送中的订单:{}", LocalDateTime.now());
        LocalDateTime time = LocalDateTime.now().plusMinutes(-60);
        List<Orders> orderList = orderMapper.getTime(Orders.DELIVERY_IN_PROGRESS, time);
        //遍历订单集合,修改状态,取消原因,取消时间
        if (orderList == null && orderList.size() > 0){
            for (Orders orders : orderList) {
                orders.setStatus(Orders.COMPLETED);  //状态改为 已完成

                orderMapper.update(orders);
            }
        }
    }
}

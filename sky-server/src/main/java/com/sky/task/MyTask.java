package com.sky.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 演示：定时任务
 */
@Component
@Slf4j
public class MyTask {
    //@Scheduled(cron = "0/5 * * * * ?")  //每隔5秒触发一次
    public void executeTask(){
        log.info("定时任务开始执行:{}",new Date());
    }
}

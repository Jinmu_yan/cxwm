package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.AliOssTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

/**
 * 演示：
 */
@Api(tags = "通用接口")
@RestController
@RequestMapping("/admin/common")
public class CommonController {
    @Autowired
    AliOssTemplate aliOssTemplate;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file) throws Exception{
        //1.获取文件后缀
        String originalFilename = file.getOriginalFilename();  //获取原始名称
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));  //点+后缀名
        //2.生成随机文件名
        String fileName = UUID.randomUUID().toString()+ suffix;
        //3.把文件上传阿里云
        String imgPath = aliOssTemplate.upload(fileName, file.getInputStream());
        //4.吧图片地址给返回
        return Result.success(imgPath);
    }
}
